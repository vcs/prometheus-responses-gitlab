FROM gitlab-registry.cern.ch/paas-tools/prometheus-responses-base:latest

LABEL maintainer "Daniel Juarez <djuarezg@cern.ch>"

#
# Copy the playbooks
#
COPY ./playbooks /var/playbooks/

RUN chown -R 1001:0 /var/playbooks && \
    chmod -R ug+rwx /var/playbooks

WORKDIR /var/playbooks/

ENTRYPOINT ["/entrypoint.sh"]

